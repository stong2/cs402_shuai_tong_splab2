#include <stdio.h>
#include <stdlib.h>
#include "person.h"
#include "readfile.h"
#include <string.h>
// compare method based on id
int cmpperson(const void *a, const void * b){
    struct person *a1 = (struct person *) a;
    struct person *b1 = (struct person *) b;
    return a1->id - b1->id;
}
// compare method based on salary
int cmpperson_salary(const void *a, const void * b){
    struct person *a1 = (struct person *) a;
    struct person *b1 = (struct person *) b;
    return b1 -> salary - a1 -> salary;
}
void printPerson(struct person * a){
     
     printf("%s ", a->firstname);
     printf("%s ", a->lastname);
     printf("%d ", a->salary);
     printf("%d\n",a->id);
     
}
// calculate the size of worker array
int len_worker(struct person * worker){
    int len = 0;
    
    while(len < 64 &&  worker[len].id != 0){
        len ++;
    }
    return len;
}
int findById(struct person * worker, int len, int input){
    int left = 0, right = len - 1;
    while(left < right){
        int mid = (left + right) / 2;
        if( (worker[mid]).id == input ){
            return mid;
        } else if((worker[mid]).id < input){
            left = mid + 1;
        } else {
            right = mid - 1;
        }
        
    }
    if( (worker[left]).id == input )
        return left;
    return -1;
}
// read from a file to init the database
int read_file(char * file_name, struct person * worker){
     int len = len_worker(worker);
     if(open_file(file_name) < 0 ){
         printf("error with open file\n");
     }
     while(not_eof()){
          struct person * tmp = &worker[len];
          if(read_int( &(tmp->id)) < 0 || read_string(tmp->firstname) < 0 
             || read_string(tmp->lastname) < 0 || read_int(&(tmp->salary)) < 0 ) {
                // printf("error with parse person info");
                 //return -1;
		 break;
             }
          len ++;
     }
     close_file();
     qsort(worker, len, sizeof(struct person), cmpperson);
     return 0;
}
void print_database(struct person *worker){
    int len = len_worker(worker);
    for(int i =0; i < len; i++){
        printPerson(&worker[i]);
    }
    printf("Number of Employees (%d)\n", len);
    return;
}
int findByLastName(struct person * worker, int len, char * input){
    for(int i = 0; i < len; i ++){
        if(strcmp(input, worker[i].lastname) == 0){
            return i;
        }
    }
    return -1;

}
int addEmployee(struct person * worker, int len, struct person * input){
    worker[len] = *input;
    len ++;
    return 0;
}
// call findbyId first , get permission , then run the left
int removeById(struct person * worker, int id){
    int len = len_worker(worker);
    int index = findById(worker, len, id);
    for(int i = index ; i < len; i++){
        worker[i] = worker[i+1];
    }
    worker[len-1].id = 0;
    worker[len-1].salary = 0;
    memset(worker[len-1].firstname, 0, 64);
    memset(worker[len-1].lastname, 0, 64);
    return 0;
    
}
int updatePerson(struct person * worker, struct person * input, int index){
    worker[index] = *input;
    int len = len_worker(worker);
    qsort(worker, len, sizeof(struct person), cmpperson);
    return 0;
}
// print m highest salary employee
int find_m_highest_salary(struct person * worker, int m){
    int len = len_worker(worker);
    if(m <= 0 || m >= len){
        printf("the range of m is not right\n");
        return -1;
    }
    qsort(worker, len, sizeof(struct person), cmpperson_salary);
    printf("the person with %d highest salary is below\n ", m);
    for(int i = 0; i < m; i++){
        printPerson(&worker[i]);
    }
    qsort(worker, len, sizeof(struct person), cmpperson);
}
// find all the people with same lastname (ignore the case)
int findAllByLastName(struct person * worker, int len, char * input){
    for(int i = 0; i < len; i ++){
        if(strcasecmp(input, worker[i].lastname) == 0){
            printPerson(&worker[i]);
        }
    }
    return 0;

}
